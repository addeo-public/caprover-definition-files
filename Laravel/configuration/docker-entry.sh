#!/bin/sh

php-fpm &
nginx -g "daemon off;"

php artisan optimize:clear
php artisan schedule:run >> /var/www/html/storage/logs/scheduler.log
composer install